# SII Backup

## Instalación

```
git clone https://git.hackware.cl/sii_backup.git
cd sii_backup
bundle install
```

## Uso

```sh
# 1. Configura datos de inicio de sesión
bundle exec bin/sii_backup config RUT_EMPRESA RUT_USUARIO CLAVE_USUARIO
# 2. Identifica folios
bundle exec bin/sii_backup folios
# 3. Descarga DTEs
bundle exec bin/sii_backup descargar
# 4. Procesa todos los DTE descargados
bundle exec bin/sii_backup procesar
```

Si algo falla, puedes pasar opciones para continuar desde donde lo dejaste:

```sh
bundle exec bin/sii_backup help <folios|descargar|procesar>
```

Para comenzar "desde 0" borra los archivos en `storage/*`.

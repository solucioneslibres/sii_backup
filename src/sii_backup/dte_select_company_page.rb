# frozen_string_literal: true

module SiiBackup
  # SII DTE select company page.
  class DteSelectCompanyPage
    URL = 'https://www1.sii.cl/Portal001/auth.html'

    def initialize(browser)
      @browser = browser
    end

    def open
      @browser.goto(URL)
      self
    end

    def select_company(rut)
      rut, dv = rut.split('-')
      rut_field.set(rut)
      dv_field.set(dv)
      @browser.send_keys(:enter)

      next_page = DteBackupPage.new(@browser, rut)
      @browser.wait_until{ next_page.loaded? }
      next_page
    end

    private

    def rut_field = @browser.text_field(name: 'RUT_EMP')
    def dv_field = @browser.text_field(name: 'DV_EMP')
  end
end

# frozen_string_literal: true

module SiiBackup
  # SII login page.
  class LoginPage
    URL = 'https://zeusr.sii.cl/AUT2000/InicioAutenticacion/IngresoRutClave.html'

    def initialize(browser)
      @browser = browser
    end

    def open
      @browser.goto(URL)
      self
    end

    def login(rut, password)
      rut_field.set(rut)
      password_field.set(password)
      @browser.send_keys(:enter)
      @browser.wait_until{ |b| b.title.include?('SII') }
    end

    private

    def rut_field = @browser.text_field(id: 'rutcntr')
    def password_field = @browser.text_field(id: 'clave')
  end
end

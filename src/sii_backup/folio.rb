# frozen_string_literal: true

require 'sii_backup'

module SiiBackup
  # Folio.
  class Folio
    include CsvStore
    include Path

    ATTRS = %w[rut folio tipo fecha].freeze

    def initialize(path, **args)
      @csv_path = storage_path("#{path}.csv")

      args.each { |k, v| instance_variable_set("@#{k}", v) }
    end

    def to_s
      "fecha: #{@fecha} - rut: #{@rut} - tipo: #{@tipo} - folio: #{@folio}"
    end
  end
end

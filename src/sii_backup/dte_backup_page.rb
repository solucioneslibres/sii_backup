# frozen_string_literal: true

require 'sii_backup'
require 'sii_backup/folio'
require 'watir/exception'
require 'watir/wait'

module SiiBackup
  # SII DTE backup page.
  class DteBackupPage
    include Path
    # URL = 'https://www1.sii.cl/cgi-bin/Portal001/lista_documentos.cgi'

    def initialize(browser, company_rut)
      @browser = browser
      @company_rut = company_rut
    end

    def loaded?
      @browser.title == 'RESPALDO DE ARCHIVOS MIPYME'
    end

    # @param [String] type RCP (received) or ENV (issued).
    def search_dtes( # rubocop:disable Metrics/AbcSize
      dte_type,
      folios_since: 1,
      folios_until: 9_999_999_999,
      date_until: nil,
      third_party_rut: nil
    )
      origen_select.set(dte_type)
      folios_since_field.set(folios_since)
      folios_until_field.set(folios_until)
      date_until_field.set(date_until)
      third_party_rut_field.set(third_party_rut)
      wait_for_page_change { submit_button.click }
    rescue Watir::Wait::TimeoutError, Watir::Exception::UnknownObjectException
      @browser.refresh
      @browser.alert.ok if @browser.alert.present?
      retry
    end

    # @param [String] issuer_rut Set if you are extracting issued (ENV) DTEs.
    def extract_folios_per_rut(dte_type)
      until first_dte.nil?
        dtes_table.tbody.rows.each do |row|
          folio = Folio.new(
            @company_rut,
            rut: dte_type == 'ENV' ? @company_rut : row.cells[0].text,
            folio: row.cells[4].text.to_i,
            tipo: dte_type,
            fecha: row.cells[5].text
          )
          folio.store
          puts(folio)
        end

        break unless next_page_link.present?

        go_next_page
      end
    end

    def download_backups(since = nil) # rubocop:disable Metrics/MethodLength,Metrics/AbcSize
      step = 20
      sorted_folios.each do |dte_type, ruts|
        next if since && since[:type] != dte_type

        ruts.each do |rut, folios|
          next if since && since[:rut] != rut

          from = since ? since[:index].to_i : 0
          since = nil if since

          while from < folios.length
            puts(
              "Descargando desde: #{dte_type} #{rut} #{from} (#{folios[from]})"
            )
            to = from + step - 1
            to -= 1 while
              folios[to].nil? || folios[to] == folios[next_from = to + 1]
            search_dtes(
              dte_type,
              folios_since: folios[from],
              folios_until: folios[to],
              third_party_rut: (rut if dte_type == 'RCP')
            )
            download_backup(dte_type, folios[from], folios[to])
            from = next_from
          end
        end
      end
    end

    private

    def sorted_folios
      csv_path = storage_path("#{@company_rut}.csv")

      folios = { 'ENV' => {}, 'RCP' => {} }

      CSV.foreach(csv_path, **CSV_OPTIONS) do |r|
        if folios[r[:tipo]].key?(r[:rut])
          folios[r[:tipo]][r[:rut]].push(r[:folio].to_i)
        else
          folios[r[:tipo]][r[:rut]] = [r[:folio].to_i]
        end
      end

      folios['ENV'].transform_values!(&:sort)
      folios['RCP'].transform_values!(&:sort)

      folios
    end

    def go_next_page
      prev_current_page_number = current_page_number
      wait_for_page_change do
        next_page_link.click if current_page_number == prev_current_page_number
      end
    rescue Watir::Wait::TimeoutError
      @browser.stop
      retry
    end

    def download_backup(dte_type, folios_since, folios_until)
      backup_button.click
      word = dte_type == 'RCP' ? 'Recibidos' : 'Emitidos'
      @browser.wait_until do
        Dir.glob(storage_path(
          "tmp/DTE_#{word}_#{@company_rut.split('-')[0]}_Folios" \
          "#{folios_since}a#{folios_until}*.xml"
        )).any?
      end
    rescue Watir::Wait::TimeoutError
      @browser.stop
      retry
    end

    def wait_for_page_change
      prev_first_dte = dtes_table.present? ? first_dte : 'not_yet'
      yield
      @browser.wait_until do
        @browser.ready_state == 'complete' && first_dte != prev_first_dte
      rescue Watir::Exception::LocatorException
        retry
      end
    end

    def first_dte
      return nil if first_row.tds.length == 1

      rut = first_row.td(index: 0).text
      doc_type = first_row.td(index: 3).text
      folio = first_row.td(index: 4).text
      "#{rut}.#{doc_type}.#{folio}"
    end

    def origen_select = @browser.select(id: 'sel_origen')
    def folios_since_field = @browser.text_field(id: 'folio')
    def folios_until_field = @browser.text_field(id: 'folioHasta')
    def date_until_field = @browser.text_field(id: 'fec_hasta')
    def third_party_rut_field = @browser.text_field(id: 'rut_recp')
    def submit_button = @browser.button(name: 'BTN_SUBMIT')
    def next_page_link = @browser.link(id: 'pagina_siguiente')
    def dtes_table = @browser.table(class: 'KnockoutFormTABLE')
    def first_row = dtes_table.tbody.rows.first
    def backup_button = @browser.button(id: 'btn_respaldar')
    def dtes_number = @browser.div(id: 'cant_reg').attribute('value').to_i

    def current_page_number
      @browser.td(class: 'KnockoutFooterTD').text
        .match('Página (\d+) de \d+')[1].to_i
    end
  end
end

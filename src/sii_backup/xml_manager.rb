# frozen_string_literal: true

require 'date'
require 'fileutils'
require 'rexml/document'
require 'rexml/xpath'
require 'sii_backup'
require 'watir/wait'

module SiiBackup
  # Operations.
  class XmlManager
    include Path

    def initialize(company_rut, dte_type)
      @company_rut = company_rut
      @dte_type = dte_type
    end

    def merge_xmls
      create_directory

      master_xml = new_xml('SetDTE')
      Dir.glob(tmp_paths_pattern).each do |tmp_path|
        tmp_xml = load_xml(tmp_path)
        tmp_xml.root.elements.each { |dte| master_xml.root.add_element(dte) }
        File.unlink(tmp_path)
      end
      File.write(master_path, master_xml.to_s)
    end

    def generate_monthly_xmls
      dtes_by_month.each do |month, dtes|
        monthly_xml = new_xml('SetDTE')
        dtes.each { |dte| monthly_xml.root.add_element(dte) }
        monthly_path = storage_path("#{@company_rut}/#{@dte_type}/#{month}.xml")
        File.open(monthly_path, 'w') { |f| monthly_xml.write(f) }
        puts("#{File.basename(monthly_path)} escrito")
      end
    end

    def generate_individual_xmls
      master_xml = load_xml(master_path)
      master_xml.root.each_element do |dte|
        individual_xml = new_xml(dte)
        file_path = storage_path(
          "#{@company_rut}/#{@dte_type}/" \
          "#{dte.elements['./Documento/Encabezado/Emisor/RUTEmisor'].text}-" \
          "#{dte.elements['./Documento/Encabezado/IdDoc/TipoDTE'].text}-" \
          "#{dte.elements['./Documento/Encabezado/IdDoc/Folio'].text}.xml"
        )
        File.open(file_path, 'w') { |f| individual_xml.write(f) }
        puts("#{File.basename(file_path)} escrito")
      end
    end

    protected

    def master_path
      storage_path("#{@company_rut}/#{@dte_type}/master.xml")
    end

    def new_xml(element)
      xml = REXML::Document.new
      xml.add(REXML::XMLDecl.new('1.0', 'ISO-8859-1'))
      xml.add_element(element)
      xml
    end

    def load_xml(path)
      REXML::Document.new(File.new(path, encoding: Encoding::ISO_8859_1))
    end

    def create_directory
      FileUtils.mkdir_p(File.dirname(master_path))
    end

    def tmp_paths_pattern
      word = @dte_type == 'RCP' ? 'Recibidos' : 'Emitidos'
      storage_path("tmp/DTE_#{word}_#{@company_rut.split('-')[0]}*.xml")
    end

    def dtes_by_month
      master_xml = load_xml(master_path)

      dtes_by_month = {}
      master_xml.root.each_element do |dte|
        date_str = dte.elements['./Documento/Encabezado/IdDoc/FchEmis'].text
        date = Date.parse(date_str)
        month = date.strftime('%Y_%m')
        dtes_by_month[month] ||= []
        dtes_by_month[month] << dte
      end
      dtes_by_month
    end
  end
end

# frozen_string_literal: true

require 'csv'

# Backup documents from www.sii.cl.
module SiiBackup
  CSV_OPTIONS = { headers: true, header_converters: :symbol }.freeze

  # Store a model class to a CSV file.
  #
  # Define: ATTRS, @csv_path
  module CsvStore
    # Store this object on a CSV file.
    def store
      csv_prepend_headers unless File.exist?(@csv_path)

      CSV.open(@csv_path, 'a', **CSV_OPTIONS) do |csv|
        csv << self.class.const_get(:ATTRS).map do |attr|
          instance_variable_get("@#{attr}")
        end
      end
    end

    protected

    def csv_prepend_headers
      FileUtils.mkdir_p(File.dirname(@csv_path))
      File.write(@csv_path, "#{self.class.const_get(:ATTRS).join(',')}\n")
    end
  end

  # Common methods for paths.
  module Path
    def storage_path(path = nil)
      return root_path("storage/#{path}") if path

      root_path('storage')
    end

    def root_path(path = nil)
      return "#{Dir.pwd}/#{path}" if path

      Dir.pwd
    end
  end
end
